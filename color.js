var colors = ['white', 'violet', 'red', 'orange', 'yellow', 'Chartreuse','aqua', 'blue', 'indigo', 'black'];
var permitt_draw = 1;
var rem;
var eraser_cursor = false;
var reset = 0;
var swatches = document.getElementsByClassName('swatch');

if(reset == false){
    canvas.style.cursor="url('pen_cur.png'), auto";
    reset = 1;
};

for(var i = 0, n = colors.length;i<n;i++){
    var swatch = document.createElement('div');
    swatch.className = 'swatch';
    swatch.style.backgroundColor = colors[i];
    swatch.addEventListener('click', setSwatch);
    document.getElementById('colors').appendChild(swatch);
}


function setColor(color){
    context.fillStyle = color;
    context.strokeStyle = color;
    var active = document.getElementsByClassName('active')[0];
    if(active){
        active.className = 'swatch';
    }
}

function setSwatch(e){
    //identify swatch
    var swatch = e.target;
    rem = swatch.style.backgroundColor;
    //set color;
    if(permitt_draw == 1){
        setColor(swatch.style.backgroundColor);
    //give active class
        swatch.className += ' active';
    }
}

setSwatch({target:document.getElementsByClassName('swatch')[4]});

// //eraser

var eraserButton = document.getElementById('eraser');
eraserButton.addEventListener('click', eraser);
function eraser(){
    draw_rect = false;
    draw_circle = false;
    permitt_draw++;
    permitt_draw = permitt_draw%2;
    if(permitt_draw == 0&&writemode===false){
        setColor('white');
    }
    eraser_cursor = true;
    if(eraser_cursor == 1&&writemode === false)canvas.style.cursor="url('eraser_cur.png'), auto";
}
// //

//pen
var penButton = document.getElementById('pen');
penButton.addEventListener('click', pen);
function pen(){
    draw_rect = false;
    draw_circle = false;
    permitt_draw = 1;
    setColor('black');
    swatch.className  +=  ' active';
    eraser_cursor = false;
    if(eraser_cursor == false&&writemode ===false)canvas.style.cursor="url('pen_cur.png'),auto";
}

//clean canvas
var cleanButton = document.getElementById('clean');
cleanButton.addEventListener('click', cleanCanvas);
function cleanCanvas(){
    // canvas.width = canvas.width;
    setRadius(defaultRad);
    setColor(rem);
    reset = 0;
    if(reset == 0){
        canvas.style.cursor="url('pen_cur.png'), auto";
        reset = 1;
    }
}
//