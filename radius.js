var setRadius = function(newRadius){
    if(newRadius < minRadius){
        newRadius = minRadius;
    }
    else if(newRadius > maxRadius){
        newRadius = maxRadius;
    }
    radius = newRadius;
    context.lineWidth = radius*2;
    //show the radius value
    radSpan.innerHTML = radius;
}

var minRadius = 0.5,
    maxRadius = 100,
    defaultRad = 5,
    interval = 5,
    radSpan = document.getElementById('radval'),
    decRad = document.getElementById('decrad'),
    incRad = document.getElementById('incrad');


decRad.addEventListener('click', function(){
    setRadius(radius-interval);
});

incRad.addEventListener('click', function(){
    setRadius(radius+interval);
});

setRadius(defaultRad);

//mode

var nowmode = document.getElementById('show');
nowmode.innerHTML = "Mode : ";
canvas.addEventListener('mousemove', mode);
function mode(){
    var text;
    if(draw_circle===true||draw_rect===true) text = "Shape mode";
    else if(writemode === true) text = "Text mode";
    else if(drawmode === true) text = "Draw mode";
    nowmode.innerHTML = "Mode : "+ text;
}