var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');

var array = new Array();
var steps = -1;
var radius = 10;
var writemode = false;
var drawmode = true;
var dragging = false;
var submitvalue = false;
var draw_rect = false;
var draw_circle = false;
//font
var text = document.getElementById('input');
var fontsize = document.getElementById('selector_fontsize');
var fonttype = document.getElementById('selector_fonttype');
var rectsize = document.getElementById('selector_rect');
var circlesize = document.getElementById('selector_circle');
////////
// canvas.width = window.innerWidth;
// canvas.height = window.innerHeight;
context.fillStyle="white";
context.fillRect(0,0,canvas.width, canvas.height);

window.onresize = function(){
    var image = context.getImagedata(0, 0, canvas.width, canvas.height);
    canvas.width = window.innerwidth;
    canvas.height = window.innerHeight;
    context.putImagedata(image, 0,0);
}
context.lineWidth = radius*2;
////////////////////undo redo
var undoButton = document.getElementById('undo');
var redoButton = document.getElementById('redo');

undoButton.addEventListener('click', undo);
redoButton.addEventListener('click', redo);

function push(){
    steps++;
    if(steps < array.length){
        array.length = steps;
    }
    array.push(canvas.toDataURL());
}

function undo(){
    if(steps > 0){
        steps--;
        var canvasPic = new Image();
        canvasPic.src = array[steps];
        canvasPic.onload = function(){
            context.drawImage(canvasPic, 0, 0, canvas.width, canvas.height);
        }
    }
}

function redo(){
    if(steps < array.length){
        steps++;
        var canvasPic = new Image();
        canvasPic.src = array[steps];
        canvasPic.onload = function(){
            context.drawImage(canvasPic, 0, 0, canvas.width, canvas.height);
        }
    }
}
/////////////////////
var putpoint = function(e){
    if(dragging&&drawmode){
        context.lineTo(e.offsetX, e.offsetY);
        context.stroke();
        context.beginPath();
        context.arc(e.offsetX, e.offsetY, radius, 0, Math.PI*2);
        context.fill();
        context.beginPath();
        context.moveTo(e.offsetX, e.offsetY);
    }
}

var engage = function(e){
    dragging = true;
    if(draw_rect === true||draw_circle===true){
        if(draw_rect){
            context.fillRect(e.offsetX, e.offsetY, 0, 0);
            context.strokeRect(e.offsetX, e.offsetY, rectsize.value, rectsize.value);
        }
        else {
            context.arc(e.offsetX, e.offsetY, circlesize.value,0, Math.PI*2)
            context.fill();
        }
    }
    else if(writemode === true&&drawmode === false&&submitvalue === true){
        var message = document.getElementById("input").value;
        context.font = fontsize.value+"px "+fonttype.value;
        context.fillText(message, e.offsetX, e.offsetY);
    }
    // if(draw_rect === false) drawmode = true;
    putpoint(e);
}

var disengage = function(){
    dragging = false;
    context.beginPath();
    push();
}

var writeButton = document.getElementById('writemode');
writeButton.addEventListener('click', putwrite);
function putwrite(){
    draw_rect = false;
    draw_circle = false;
    writemode = true;
    drawmode = false;
    submitvalue = true;
    canvas.style.cursor="crosshair";
}

var drawButton = document.getElementById('drawmode');
drawButton.addEventListener('click', putdraw);
function putdraw(){
    draw_rect = false;
    draw_circle = false;
    writemode = false;
    drawmode = true;
    submitvalue = false;
    setColor('black');
    canvas.style.cursor="url('pen_cur.png'), auto";
}
//upload
var imgupload = document.getElementById('upload');
imgupload.addEventListener('change', output, false);
function output(e){
    var loader = new FileReader();
    loader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            context.drawImage(img, 0, 0);
            context.lineWidth = radius*2;
        }
        img.src = event.target.result;
    }
    loader.readAsDataURL(e.target.files[0]);
    
}

//shape
var rectButton = document.getElementById('rect');
rectButton.addEventListener('click', drawrect);
function drawrect(){
    draw_rect = true;
    drawmode = false;
    writemode = false;
    draw_circle = false;
    canvas.style.cursor="move";
}

var circleButton = document.getElementById('circle');
circleButton.addEventListener('click', drawcircle);
function drawcircle(){
    draw_circle = true;
    drawmode = false;
    writemode = false;
    draw_rect = false;
    canvas.style.cursor="move";
}
canvas.addEventListener('mousedown', engage);
canvas.addEventListener('mousemove', putpoint);
canvas.addEventListener('mouseup', disengage);
push();